<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">
    <link rel="stylesheet" href="style.css" />
    <link href="https://fonts.googleapis.com/css2?family=Poppins&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.1/dist/css/bootstrap.min.css">
    <title><?php echo $judul;?></title>
  </head>
  <body>
  <nav class="navbar navbar-expand-lg">
    <div class="container-fluid">
        <a class="navbar-brand" href="#">
            <img src="2.png" alt="Bootstrap" width="90" height="auto">
        </a>
    </div>
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
          </button>
        <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <a class="nav-item">
                    <a class="nav-link active" aria-current="page" href="#"><b>Beranda</b></a>
                </a>
                <a class="nav-item">
                    <a class="nav-link active" href="#"><b>Artikel</b></a>
                </a>
                <a class="nav-item">
                    <a class="nav-link active" href="#"><b>Layanan</b></a>
                </a>
                <a class="nav-item">
                    <a class="nav-link active" href="#"><b>Login</b></a>
                </a>
            </ul>
        </div>
    </div>
</nav>
<body>
    <div class="container">
        <p class="Title-landing-page">DAENG WEBSITE</p>
        <p class="subtitle-landing-page">Solusi Terbaik Kebutuhan </p>
        <p class="subtitle-landing-page2">Anda di Era Digital.</p>
        <p class="Title-qoutes">Website adalah Perusahaan penyedia layanan pembuatan</p>
        <p class="Title-qoutes2">website sesuai dengan kebutuhan bisnis anda. Murah, cepat dan berkualitas.</p>
        <button class="button" type="button"><b>Cara Order ></b></button>
        <img class="gambar1" src="Group 1.png" alt="#">
    </div>
    <div>
        <h1 class="Login-website">DAENG WEBSITE
            <h1 class="Login-website2">LOGIN</h1>
        </h1>
    </div>
    <div class="Usernamepasword">
        <input type="text" class="Username" placeholder="Username atau Email">
        <br><input type="password" class="Password" placeholder="Password">
        <br><input type="submit" value="Login" class="submit1">
        <div class="Oror">
            Or
        </div>
    </div>
    <div>
        <button type="submit" class="submit2"><i><svg xmlns="http://www.w3.org/2000/svg" width="35" color="#3b5998" height="35" fill="currentColor" class="bi bi-facebook" viewBox="0 0 16 16">
        <path d="M16 8.049c0-4.446-3.582-8.05-8-8.05C3.58 0-.002 3.603-.002 8.05c0 4.017 2.926 7.347 6.75 7.951v-5.625h-2.03V8.05H6.75V6.275c0-2.017 1.195-3.131 3.022-3.131.876 0 1.791.157 1.791.157v1.98h-1.009c-.993 0-1.303.621-1.303 1.258v1.51h2.218l-.354 2.326H9.25V16c3.824-.604 6.75-3.934 6.75-7.951z"/>
      </svg></i> Login with Facebook</button>
        <button type="submit" class="submit3"><i><img src="google1.png" alt="#" width="35px" height="35"></i> Login with Google</button>
    </div>
    <div>
        <img src="lef1.png" alt="#" class="lef1" height="350px">
        <img src="lef2.png" alt="#" class="lef2" height="350px">
    </div>
    <div>
        <p class="tanya1">Website Apa Yang Anda Butuhkan?</p>
        <img class="promosi1" src="promosi.png" alt="#" width="80%">
        <p class="tanya2">Tidak menemukan kategori website sesuai yang Anda inginkan?</p>
        <p class="tanya3">Konsultasikan dengan kami kebutuhan website Anda. Kami akan berikan pilihan</p>
        <p class="tanya4">dan masukan yang tepat untuk website Anda.</p>
        <button class="button5" type="button"><b>Konsultasi ></b></button>
    </div>

    <div>
        <p class="harga1">Pilih Paket Website Langsung Jadi</p>
        <img src="harga2.png" alt="#" class="harga2" width="53%" height="53%">
        <button class="button6" type="button"><b>Detail Paket ></b></button>
    </div>
    <div>
        <img src="lef3.png" alt="#" class="lef3" width="20%" height="20%">
        <img src="lef4.png" alt="#" class="lef4" width="20%" height="20%">
    </div>
    <div>
        <p class="Folio1">Portofolio Kami</p>
        <img src="tokoon.png" alt="#" class="tokoon" width="20%" height="20%">
        <img src="company.png" alt="#" class="company" width="21%" height="21%">
        <br><button class="buttontok" type="button"><b>Detail ></b></button>
        <button class="buttonny" type="button"><b>Detail ></b></button>
        <br><img src="beon.png" alt="#" class="beon" width="20%" height="20%">
        <img src="travel.png" alt="#" class="travel" width="20%" height="20%">
        <br><button class="buttonbe" type="button"><b>Detail ></b></button>
        <button class="buttonvel" type="button"><b>Detail ></b></button>
    </div>